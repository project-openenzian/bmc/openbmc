DESCRIPTION = "EnzianBMC NOR flash image"

inherit deploy

LICENSE = "BSD-2-Clause"
DEPENDS += "u-boot"
NOR_IMAGE_NAME ?= "nor-${MACHINE}.bin"

do_compile () {
    # FSBL
    dd if=${STAGING_DIR_HOST}/boot/boot.bin of=${B}/${NOR_IMAGE_NAME} bs=16777216
    # Boot image (U-Boot & bitstream)
    dd if=${STAGING_DIR_HOST}/boot/u-boot.itb of=${B}/${NOR_IMAGE_NAME} bs=16777216 seek=1
}

do_deploy () {
    install -m644 -D ${B}/${NOR_IMAGE_NAME} ${DEPLOYDIR}/${NOR_IMAGE_NAME}
}
addtask deploy after do_compile
