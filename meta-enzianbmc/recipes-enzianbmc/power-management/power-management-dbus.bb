require power-management.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

RDEPENDS:${PN} += "power-management-libs"

SCRIPT_NAME = "src/start_services.py"
INSTALL_NAME = "start-enzian-services"

DBUS_SERVICE:${PN} = "               \
    systems.enzian.Gpio.service      \
    systems.enzian.Power.service     \
    systems.enzian.Fault.service     \
    systems.enzian.Telemetry.service \
"

inherit obmc-phosphor-pydbus-service
