LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0901edcdd399306f84b6ecb7cc3f94f0"

SRCREV = "c6c60e588458b21ec0b97b835c12fd900aa29e8e"
PV = "1.0+git${SRCPV}"
SRC_URI = "git://git@gitlab.inf.ethz.ch/project-openenzian/bmc/power-management-stack.git;branch=main;protocol=https"

S = "${WORKDIR}/git"
