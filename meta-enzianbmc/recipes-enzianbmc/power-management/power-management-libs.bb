require power-management.inc

inherit setuptools3

RDEPENDS:${PN} += "   \
    libgpiod-python   \
    python3-ctypes    \
    python3-dbus      \
    python3-mmap      \
    python3-pygobject \
    python3-smbus     \
"
