require power-management.inc

SCRIPT_NAME = "src/shell.py"
INSTALL_NAME = "enzian-shell"

RDEPENDS:${PN} = "power-management-libs power-management-dbus"

# For backwards compatibility
OLD_BASE_DIR = "/power-manager"
OLD_SHELL = "${OLD_BASE_DIR}/shell.py"
FILES:${PN} += "${OLD_SHELL}"

do_install:append() {
    # install the script
    install -d ${D}${sbindir}
    install -m 0755 ${S}/${SCRIPT_NAME} ${D}${sbindir}/${INSTALL_NAME}

    # For backwards compatibility
    install -d ${D}${OLD_BASE_DIR}
    ln -s -r ${D}${sbindir}/${INSTALL_NAME} ${D}${OLD_SHELL}
}
