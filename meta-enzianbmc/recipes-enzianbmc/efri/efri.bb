LICENSE = "BSD-2-Clause"

RDEPENDS:${PN} += "\
    python3-json \
    python3-pyserial \
    python3-scanf \
"

FILES:${PN} += "${sbindir}"

do_install:append() {
    install -d ${D}${sbindir}
}
