DESCRIPTION = "Enzian BMC XU5 FPGA bitstream"

inherit deploy

PROVIDES = "enzianbmc-fpga"

MODULE_VARIANT ?= "?"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=72b914f619f0b9d9cb57a908b053b693"
SRCREV = "2d63c96c"
PV = "1.0+git+${SRCREV}"
SRC_URI = "https://gitlab.ethz.ch/api/v4/projects/47289/packages/generic/fpga-xu5/${SRCREV}/enzianbmc-fpga-XU5-${MODULE_VARIANT}.tar.gz"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/sysroot-only/
    # Raw bitstream to integrate in boot image
    install -m644 ${S}/Enzian_BMC_XU5.bin ${D}/sysroot-only/bitstream.bin
    # Initialization code for PS, needed by U-Boot SPL
    install -m644 ${S}/psu_init_gpl.h ${D}/sysroot-only/
    install -m644 ${S}/psu_init_gpl.c ${D}/sysroot-only/
}

do_deploy () {
    # Bitstream for debugging
    install -m644 ${S}/Enzian_BMC_XU5.bit ${DEPLOYDIR}/bitstream-${MACHINE}.bit
    # PS init TCL script for debugging
    install -m644 ${S}/psu_init.tcl ${DEPLOYDIR}/psu_init-${MACHINE}.tcl
}
addtask deploy after do_compile
