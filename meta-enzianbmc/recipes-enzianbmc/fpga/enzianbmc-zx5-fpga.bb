DESCRIPTION = "Enzian BMC ZX5 FPGA bitstream"

inherit deploy

PROVIDES = "enzianbmc-fpga"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=72b914f619f0b9d9cb57a908b053b693"
SRCREV = "a99181d0"
PV = "1.0+git+${SRCREV}"
SRC_URI = "https://gitlab.ethz.ch/api/v4/projects/47289/packages/generic/fpga-zx5/${SRCREV}/enzianbmc-fpga-ZX5.tar.gz"
SRC_URI[sha256sum] = "f7a7e42ba45dda1e4a7696f4a10eae60a7f43231eaaff242a31c9b896fcf233e"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/sysroot-only/
    # Raw bitstream to integrate in boot image
    install -m644 ${S}/Enzian_BMC_ZX5.bin ${D}/sysroot-only/bitstream.bin
    # Initialization code for PS, needed by U-Boot SPL
    install -m644 ${S}/ps7_init_gpl.h ${D}/sysroot-only/
    install -m644 ${S}/ps7_init_gpl.c ${D}/sysroot-only/
}

do_deploy () {
    # Bitstream for debugging
    install -m644 ${S}/Enzian_BMC_ZX5.bit ${DEPLOYDIR}/bitstream-${MACHINE}.bit
    # PS init TCL script for debugging
    install -m644 ${S}/ps7_init.tcl ${DEPLOYDIR}/ps7_init-${MACHINE}.tcl
}
addtask deploy after do_compile
