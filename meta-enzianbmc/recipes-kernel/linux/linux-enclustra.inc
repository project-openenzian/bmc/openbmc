DESCRIPTION = "Linux kernel from Enclustra BSP"
SECTION = "kernel"
LICENSE = "GPL-2.0-only"

KCONFIG_MODE="--alldefconfig"

require recipes-kernel/linux/linux-yocto.inc

KERNELURI ?= "git://github.com/enclustra-bsp/xilinx-linux.git;name=machine;branch=master;protocol=https"
YOCTO_META ?= "git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-${META_VERSION};destsuffix=yocto-kmeta"
SRC_URI = "${KERNELURI} ${YOCTO_META}"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://xilinx-gpio-dual-get.patch"

SRC_URI += "file://enzianbmc_defconfig"

SRC_URI += "file://enzianbmc-zx5.dts;subdir=git/arch/arm/boot/dts"
SRC_URI += "file://enzianbmc-xu5.dtsi;subdir=git/arch/arm64/boot/dts"
SRC_URI += "file://enzianbmc-xu5-5ev.dts;subdir=git/arch/arm64/boot/dts"
SRC_URI += "file://enzianbmc-xu5-4ev.dts;subdir=git/arch/arm64/boot/dts"

PV = "${LINUX_VERSION}"
LINUX_VERSION_EXTENSION = "-xilinx"
