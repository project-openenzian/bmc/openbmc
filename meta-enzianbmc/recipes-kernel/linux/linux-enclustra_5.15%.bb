LINUX_VERSION ?= "5.15.19"
META_VERSION ?= "5.15"

SRCREV_machine = "3701a4ff4efdb2a2d86ebac61340dfa42f1dbc2a"
SRCREV_meta = "5f6249ab6fc0f2ac02d745c815adfbf2ad2f92fa"

require linux-enclustra.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

PACKAGE_ARCH = "${MACHINE_ARCH}"
