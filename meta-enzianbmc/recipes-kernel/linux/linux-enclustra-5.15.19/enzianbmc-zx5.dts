/* The includes in this device tree file are the ones
 * that the enclustra-bsb-xilinx uses
 * (except for the PE1 carrier board one, that we don't need)
 * The rest is custom for the Enzian BMC
 */

/dts-v1/;

#include "ME-ZX5-15-2I-D10.dtsi"

/ {
    chosen {
        bootargs = "console=ttyPS0,115200 rw earlyprintk rootwait ubi.mtd=nand-rootfs rootfstype=ubifs root=ubi0:enzianbmc-zx5-rootfs";
        stdout-path = "serial0:115200n8";
    };
};

/* From zynq_enclustra_nand_parts.dtsi
 * partition layout adapted
 */
&smcc {
	status = "okay";
};

&nfc0 {
	status = "okay";

	nand@0 {
		reg = <0>;
        /* Enclustra puts these on the parent
         * but need to be here
         */
        nand-ecc-mode = "on-die";
	    nand-bus-width = <0x8>;

        partitions {
            compatible = "fixed-partitions";
		    #address-cells = <1>;
		    #size-cells = <1>;

            partition@0 {
                label = "nand-linux";
                reg = <0x0000000 0x2000000>;
            };
            partition@2000000 {
                label = "nand-devicetree";
                reg = <0x2000000 0x80000>;
            };
            partition@2080000 {
                label = "nand-bootscript";
                reg = <0x2080000 0x80000>;
            };
            partition@2100000 {
                label = "nand-rootfs";
                reg = <0x2100000 0x1DF00000>;
            };
        };
	};
};

/********************** 
 * Programmable logic *
 **********************/
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/interrupt-controller/irq.h>

&fpga_full {
    /* We configure the FPGA before OS boot */
    external-fpga-config;

    ncis: ethernet@40e00000 {
        compatible = "xlnx,xps-ethernetlite-3.00.a";
        status = "disabled";
        interrupt-parent = <&intc>;
        interrupts = <0 59 IRQ_TYPE_EDGE_RISING>;
        reg = <0x40e00000 0x10000>;
        local-mac-address = [02 0a 35 00 00 01];
        xlnx,rx-ping-pong = <0x1>;
        xlnx,tx-ping-pong = <0x1>;
    };

    gpio_bmc: gpio@41200000 {
        #gpio-cells = <2>;
        compatible = "xlnx,xps-gpio-1.00.a";
        gpio-controller;
        clocks = <&clkc 42>;
        clock-names = "s_axi_aclk";
        interrupt-parent = <&intc>;
        interrupts = <0 56 IRQ_TYPE_LEVEL_HIGH>;
        reg = <0x41200000 0x1000>;
        xlnx,is-dual = <0x1>;
        xlnx,dout-default = <0x0>;
        xlnx,dout-default-2 = <0x0>;
        xlnx,gpio-width = <32>;
        xlnx,gpio2-width = <32>;
        xlnx,tri-default = <0xffffffff>;
        xlnx,tri-default-2 = <0xffffffff>;
        gpio-line-names =
            /* Bank 0 */
            "F_SSCONF_INIT_B",
            "F_SSCONF_DONE",
            "F_SSCONF_PROG_B",
            "C_RESET_OUT_N",
            "B_CLOCK_BLOL",
            "B_CLOCK_CLOL",
            "B_CLOCK_FLOL",
            "",
            "B_USB2_OC_N",
            "B_PSUP_ON",
            "C_D_EVENT_N",
            "F_D_EVENT_N",
            "",
            "",
            "",
            "",
            "B_USERIO_SW1_N",
            "B_USERIO_SW2_N",
            "B_USERIO_SW3_N",
            "B_USERIO_SW4_N",
            "B_USERIO_SW5_N",
            "B_USERIO_SW6_N",
            "B_USERIO_SW7_N",
            "B_USERIO_SW8_N",
            "B_PWR_ALERT_N",
            "B_SEQ_ALERT_N",
            "B_PSU_ALERT_N",
            "B_FAN_ALERT_N",
            "B_FAN_FAULT_N",
            "",
            "",
            "",
            /* Bank 1 */
            "B_SPI_SEL_N",
            "",
            "B_FPIO_PWR_LED",
            "B_FPIO_PWR_SW_N",
            "B_FPIO_RST_SW_N",
            "B_FPIO_SID_SW_N",
            "B_FPIO_1WIRE",
            "B_FPIO_NMI_SW_N",
            "B_FPIO_SID_LED",
            "B_FPIO_F1_LED",
            "B_FPIO_F2_LED",
            "B_FPIO_NIC1_LED",
            "B_FPIO_CI_SW_N",
            "B_FPIO_NIC2_LED",
            "",
            "",
            "B_USERIO_LED1",
            "B_USERIO_LED2",
            "B_USERIO_LED3",
            "B_USERIO_LED4",
            "B_USERIO_LED5",
            "B_USERIO_LED6",
            "B_USERIO_LED7",
            "B_USERIO_LED8",
            "C_RESET_N",
            "C_PLL_DCOK",
            "B_OCI2_LNK1",
            "B_OCI3_LNK1",
            "B_C2C_NMI",
            "B_FMCC_SEL",
            "B_FAN_RESET_N",
            "B_USB2_EN";
    };
    
    gpio_dv: gpio_dv@41210000 {
        #gpio-cells = <2>;
        compatible = "xlnx,xps-gpio-1.00.a";
        gpio-controller;
        clocks = <&clkc 42>;
        clock-names = "s_axi_aclk";
        reg = <0x41210000 0x1000>;
        xlnx,is-dual = <0x1>;
        xlnx,dout-default = <0xff>;
        xlnx,dout-default-2 = <0xff>;
        xlnx,gpio-width = <8>;
        xlnx,gpio2-width = <8>;
        xlnx,tri-default = <0xffffffff>;
        xlnx,tri-default-2 = <0xffffffff>;
        gpio-line-names =
            /* Bank 0 */
            "B_CDV_VID0",
            "B_CDV_VID1",
            "B_CDV_VID2",
            "B_CDV_VID3",
            "B_CDV_VID4",
            "B_CDV_VID5",
            "B_CDV_VID6",
            "B_CDV_VID7",
            /* Bank 1 */
            "B_FDV_VID0",
            "B_FDV_VID1",
            "B_FDV_VID2",
            "B_FDV_VID3",
            "B_FDV_VID4",
            "B_FDV_VID5",
            "B_FDV_VID6",
            "B_FDV_VID7";
    };

    gpio_i2c: gpio_i2c@41220000 {
        #gpio-cells = <2>;
        compatible = "xlnx,xps-gpio-1.00.a";
        gpio-controller;
        clocks = <&clkc 42>;
        clock-names = "s_axi_aclk";
        interrupt-parent = <&intc>;
        interrupts = <0 33 IRQ_TYPE_LEVEL_HIGH>;
        reg = <0x41220000 0x1000>;
        xlnx,is-dual = <0x0>;
        xlnx,dout-default = <0x0>;
        xlnx,gpio-width = <10>;
        xlnx,tri-default = <0xffffffff>;
        gpio-line-names =
            /* Bank 0 */
            "B_SEQ_I2C_SDA",
            "B_SEQ_I2C_SCL",
            "B_PSU_I2C_SDA",
            "B_PSU_I2C_SCL",
            "B_CLOCK_I2C_SDA",
            "B_CLOCK_I2C_SCL",
            "B_FPIO_I2C_SDA",
            "B_FPIO_I2C_SCL",
            "B_PWR_FAN_I2C_SDA",
            "B_PWR_FAN_I2C_SCL";
    };

    seq_i2c {
        compatible = "i2c-gpio";
        #address-cells = <1>;
        #size-cells = <0>;
        i2c-gpio,delay-us = <5>;
        sda-gpios = <&gpio_i2c 0 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
        scl-gpios = <&gpio_i2c 1 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
    };

    psu_i2c {
        compatible = "i2c-gpio";
        #address-cells = <1>;
        #size-cells = <0>;
        i2c-gpio,delay-us = <5>;
        sda-gpios = <&gpio_i2c 2 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
        scl-gpios = <&gpio_i2c 3 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
    };

    clock_i2c {
        compatible = "i2c-gpio";
        #address-cells = <1>;
        #size-cells = <0>;
        i2c-gpio,delay-us = <5>;
        sda-gpios = <&gpio_i2c 4 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
        scl-gpios = <&gpio_i2c 5 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
    };

    fpio_i2c {
        compatible = "i2c-gpio";
        #address-cells = <1>;
        #size-cells = <0>;
        i2c-gpio,delay-us = <5>;
        sda-gpios = <&gpio_i2c 6 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
        scl-gpios = <&gpio_i2c 7 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
    };

    pwr_fan_i2c {
        compatible = "i2c-gpio";
        #address-cells = <1>;
        #size-cells = <0>;
        i2c-gpio,delay-us = <5>;
        sda-gpios = <&gpio_i2c 8 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
        scl-gpios = <&gpio_i2c 9 (GPIO_ACTIVE_HIGH|GPIO_OPEN_DRAIN)>;
    };

    gpio_led: gpio_led@41230000 {
        #gpio-cells = <2>;
        compatible = "xlnx,xps-gpio-1.00.a";
        gpio-controller;
        clocks = <&clkc 42>;
        clock-names = "s_axi_aclk";
        reg = <0x41230000 0x1000>;
        xlnx,all-inputs = <0x0>;
        xlnx,all-outputs = <0x1>;
        xlnx,dout-default = <0x00000001>;
        xlnx,gpio-width = <1>;
        xlnx,is-dual = <0x0>;
        xlnx,tri-default = <0xffffffff>;
        gpio-line-names =
            "ZX5_LED_N";
    };

    flash_spi: spi@41e00000 {
        compatible = "xlnx,xps-spi-2.00.a";
        interrupt-parent = <&intc>;
        interrupts = <0 57 IRQ_TYPE_LEVEL_HIGH>;
        reg = <0x41e00000 0x1000>;
        fifo-size = <256>;
        #address-cells = <1>;
        #size-cells = <0>;

        enzian_flash@0 {
            compatible = "n25q128a13";
            reg = <0>;
            spi-max-frequency = <50000000>;

            partitions {
                compatible = "fixed-partitions";
                #address-cells = <1>;
                #size-cells = <1>;

                partition@0 {
                    label = "enzian-bdk";
                    reg = <0x000000 0x1000000>;
                };
            };
        };
    };

    fpga_spi: spi@41e10000 {
        compatible = "xlnx,xps-spi-2.00.b";
        interrupt-parent = <&intc>;
        interrupts = <0 58 IRQ_TYPE_LEVEL_HIGH>;
        reg = <0x41e10000 0x1000>;
        fifo-size = <256>;
        #address-cells = <1>;
        #size-cells = <0>;

        ultrascale_fpga: fpga-mgr@0 {
            compatible = "xlnx,fpga-slave-serial";
            spi-max-frequency = <100000000>;
            reg = <0>;
            init-b-gpios = <&gpio_bmc 0 GPIO_ACTIVE_LOW>;
            done-gpios = <&gpio_bmc 1 GPIO_ACTIVE_HIGH>;
            prog_b-gpios = <&gpio_bmc 2 GPIO_ACTIVE_LOW>;
        };
    };

    uart1@43c40000 {
        compatible = "ns16550";
        status = "disabled";
        clock-frequency = <100000000>;
        reg = <0x43c40000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 53 IRQ_TYPE_LEVEL_HIGH>;
    };

    uart2@43c50000 {
        compatible = "ns16550";
        clock-frequency = <100000000>;
        reg = <0x43c50000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 54 IRQ_TYPE_LEVEL_HIGH>;
    };

    uart3@43c60000 {
        compatible = "ns16550";
        status = "disabled";
        clock-frequency = <100000000>;
        reg = <0x43c60000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 55 IRQ_TYPE_LEVEL_HIGH>;
    };

    cuart0@43c10000 {
        compatible = "ns16550";
        status = "disabled";
        clock-frequency = <100000000>;
        reg = <0x43c10000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 35 IRQ_TYPE_LEVEL_HIGH>;
    };

    cuart1@43c20000 {
        compatible = "ns16550";
        clock-frequency = <100000000>;
        reg = <0x43c20000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 36 IRQ_TYPE_LEVEL_HIGH>;
    };

    fuart@43c00000 {
        compatible = "ns16550";
        status = "disabled";
        clock-frequency = <100000000>;
        reg = <0x43c00000 0x10000>;
        reg-offset = <0x1000>;
        reg-shift = <2>;
        reg-io-width = <4>;
        interrupt-parent = <&intc>;
        interrupts = <0 34 IRQ_TYPE_LEVEL_HIGH>;
    };
};
