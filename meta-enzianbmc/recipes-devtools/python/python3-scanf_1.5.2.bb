SUMMARY = "A small scanf implementation for python"
HOMEPAGE = "https://github.com/joshburnett/scanf"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=a6518f361aea975d0b0e6c4badd5af0d"

# Released version doesn't include license file, get it from GitHub
SRC_URI += "https://raw.githubusercontent.com/joshburnett/scanf/v1.5.2/LICENSE;name=license"
SRC_URI[license.md5sum] = "a6518f361aea975d0b0e6c4badd5af0d"

SRC_URI[sha256sum] = "57633440a02a138cd14b693d09270af0a03bb017e8d4cfd248c7988b31b8cb81"

inherit pypi setuptools3
