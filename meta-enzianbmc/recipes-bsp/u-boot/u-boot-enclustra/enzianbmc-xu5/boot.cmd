# first load all the images from current mmcdev
load mmc ${mmcdev} ${kernel_loadaddr} ${kernel_file} || exit;
load mmc ${mmcdev} ${devicetree_loadaddr} ${devicetree_file} || exit;

# setup mmc bootargs
run mmc_args;

# now attempt to boot
booti ${kernel_loadaddr} - ${devicetree_loadaddr};
