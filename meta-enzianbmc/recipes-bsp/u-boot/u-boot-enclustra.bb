require u-boot-common-enclustra.inc
require recipes-bsp/u-boot/u-boot.inc

PROVIDES += "u-boot"

DEPENDS += "enzianbmc-fpga"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
PACKAGE_ARCH = "${MACHINE_ARCH}"

SRC_URI += "file://fix-zynqmp-dram-ecc-initialization.patch"
SRC_URI += "file://remove-hardcoded-enclustra-config.patch"

SRC_URI += "file://enzianbmc_zx5_defconfig;subdir=${S}/configs"
SRC_URI += "file://enzianbmc_xu5_4ev_defconfig;subdir=${S}/configs"
SRC_URI += "file://enzianbmc_xu5_5ev_defconfig;subdir=${S}/configs"

SRC_URI += "file://enzianbmc-zx5.dts;subdir=${S}/arch/arm/dts"
SRC_URI += "file://enzianbmc-xu5.dtsi;subdir=${S}/arch/arm/dts"
SRC_URI += "file://enzianbmc-xu5-4ev.dts;subdir=${S}/arch/arm/dts"
SRC_URI += "file://enzianbmc-xu5-5ev.dts;subdir=${S}/arch/arm/dts"
SRC_URI += "file://dt-makefile.patch"

SRC_URI += "file://enzianbmc.its"
SRC_URI += "file://u-boot-env.txt"

SRC_URI+= "file://boot.cmd"

# Patch only needed for ZX5
SRC_URI:append:enzianbmc-zx5 = "\
    file://initialize-UART-at-least-once.patch  \
"

# ZynqMPSoC firmware, XU5 only
SRC_URI:append:enzianbmc-xu5 = "\
    file://bl31.bin             \
    file://pmu_cfg_obj.bin      \
    file://pmufw.bin            \
"

SYSROOT_DIRS += "/boot"
