# Enable systemd-networkd to set the hostname without needing polkit
# This is required to make dynamic hostnames work
# To enable dynamic hostnames, add `hostname:pn-base-files = ""` to your config
PACKAGECONFIG += "polkit_hostnamed_fallback"
