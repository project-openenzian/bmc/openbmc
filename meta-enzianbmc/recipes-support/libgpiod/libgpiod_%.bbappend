# Upstream recipe inherits only python3native
# This will result in python3-config advertising the
# path for the C headers (and probably others) for the build host
# but we need the ones for the target
inherit python3targetconfig

PACKAGECONFIG += "python3"
