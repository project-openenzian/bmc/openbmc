MACHINEOVERRIDES =. "enzianbmc-xu5:"

PREFERRED_PROVIDER_virtual/bootloader ?= "u-boot-enclustra"
PREFERRED_PROVIDER_u-boot ?= "u-boot-enclustra"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-enclustra"
PREFERRED_VERSION_linux-enclustra ?= "5.15%"

MACHINE_FEATURES = "ext2 vfat usbhost"

DEFAULTTUNE ?= "cortexa53"
require conf/machine/include/arm/armv8a/tune-cortexa53.inc

SPL_BINARY = "spl/boot.bin"
SPL_SUFFIX = "bin"
UBOOT_SUFFIX = "itb"
UBOOT_ELF = "u-boot.elf"
UBOOT_ENV = "boot"
UBOOT_ENV_SUFFIX = "scr"

SERIAL_CONSOLES = "115200;ttyPS0 115200;ttyPS1"

KERNEL_IMAGETYPE = "Image"

IMAGE_FSTYPES = "ext4.gz"
