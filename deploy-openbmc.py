#!/usr/bin/python3

import argparse
import re
import sys
import os
import shutil

NUM_KEEP_OLD = 5

LATEST_TAG = "enzianbmc-latest"
STABLE_TAG = "enzianbmc-stable"
OLDSTABLE_TAG = "enzianbmc-oldstable"
DEV_TAG = "enzianbmc-dev"

VERSION_REGEX = re.compile(r"^enzianbmc-[0-9a-f]{8}$")

def get_version(tag):
    if os.path.islink(tag):
        return os.readlink(tag)
    else:
        return None

def main():
    parser = argparse.ArgumentParser(description="Deploy Enzian BMC firmware to TFTP server")
    parser.add_argument("directory", metavar="DIR", type=str,
        help="The directory to deploy to."
    )
    parser.add_argument("git_hash", metavar="HASH", type=str,
        help="The Git hash of the version we're deploying."
    )
    parser.add_argument("--not-latest", dest="latest", action="store_false", default=True,
        help="Whether we are deploying a version that isn't the latest one"
    )
    parser.add_argument("--stable", "-s", action="store_true", default=False,
        help="Whether we are deploying a stable version"
    )
    parser.add_argument("--dev", action="store_true", default=False,
        help="Whether we are deploying a development version"
    )
    parser.add_argument("--dry-run", "-d", dest="execute", action="store_false", default=True,
        help="See what would happen but don't change anything."
    )

    args = parser.parse_args()

    os.chdir(args.directory)

    current_version = f"enzianbmc-{args.git_hash}"
    print(f"Deploying {current_version}")
    if not os.path.isdir(current_version):
        print(f"Version that should be deployed has not been copied to target!", file=sys.stderr)
        exit(1)

    # If we deploy a dev version, we don't touch anything else
    if args.dev:
        dev_version = get_version(DEV_TAG)
        print(f"Current dev: {dev_version}")
        if args.execute:
            if dev_version is not None:
                os.unlink(DEV_TAG)
                shutil.rmtree(dev_version)
            os.symlink(current_version, DEV_TAG)
        exit(0)

    latest_version = get_version(LATEST_TAG)
    stable_version = get_version(STABLE_TAG)
    oldstable_version = get_version(OLDSTABLE_TAG)
    print(f"Current latest: {latest_version}")
    print(f"Current stable: {stable_version}")
    print(f"Current oldstable: {oldstable_version}")        

    if args.latest:
        print("Re-linking latest version")
        if args.execute:
            if latest_version is not None:
                os.unlink(LATEST_TAG)
            os.symlink(current_version, LATEST_TAG)
        latest_version = current_version

    if args.stable:
        print("Re-linking oldstable version")
        if args.execute:
            if oldstable_version is not None:
                os.unlink(OLDSTABLE_TAG)
            if stable_version is not None:
                os.symlink(stable_version, OLDSTABLE_TAG)
        oldstable_version = stable_version

        print("Re-linking stable version")
        if args.execute:
            if stable_version is not None:
                os.unlink(STABLE_TAG)
            os.symlink(current_version, STABLE_TAG)
        stable_version = current_version

    old_versions = []
    with os.scandir() as dirlist:
        for entry in dirlist:
            if not entry.is_dir(follow_symlinks=False):
                # Not a directory, skip
                continue
            if VERSION_REGEX.match(entry.name) is None:
                # Not an OpenBMC firmware directory, skip
                continue

            # If it's not the latest version, stable or oldstable
            # it's a candidate for deletion
            if entry.name not in [current_version, latest_version, stable_version, oldstable_version]:
                old_versions.append(entry.name)
    

    # Sort newest first
    old_versions.sort(key=os.path.getmtime, reverse=True)

    # Remove all but the newest few
    for v in old_versions[NUM_KEEP_OLD:]:
        print(f"Removing {v}")
        if args.execute:
            shutil.rmtree(v)


if __name__ == "__main__":
    main()
